# Dotfiles
Primär für MacOS, alle anderen Betriebssysteme müssen noch geprüft und angepasst werden.

## Voraussetzungen
- Homebrew installiert (https://brew.sh/)
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

- zsh installiert
```
brew install zsh
```

- zplug installiert (https://github.com/zplug/zplug)
```
brew install zplug
```

- Fonts installiert
 // TODO

## Installation

- clone Repository
```
git clone https://gitlab.com/brandtkollmar/dotfiles.git .dotfiles
```
- erstelle Symlinks
```
ln -s ~/.dotfiles/zsh/.zshrc ~/.zshrc
```
- Konfiguration sourcen
```
source ~/.zshrc
```
