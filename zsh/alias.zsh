setopt COMPLETE_ALIASES

alias l='ls -tF'     # type, info
alias la='ls -tFA'   # type, info, all
alias ll='ls -ltFAh' # long list, type, info, all human readable

alias dud='du -d 1 -h'
alias duf='du -sh *'
alias fd='find . -type d -name'
alias ff='find . -type f -name'

alias port='lsof -nP -iTCP | grep LISTEN | fzf'

alias javaversions='/usr/libexec/java_home -V'

alias kraken='open -na GitKraken --args -p "$(git rev-parse --show-toplevel)"'
