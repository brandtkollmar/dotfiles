# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


export TERM="xterm-256color"
export BASEDIR="$HOME"
export ZSH_CONF="$BASEDIR/.dotfiles/zsh"
export ZPLUG="$HOME/.zplug"


# zplug
export ZSH_CACHE_DIR="$HOME/.cache/zsh"
if [[ ! -d "$ZSH_CACHE_DIR" ]]; then
    mkdir -p "$ZSH_CACHE_DIR"
fi

export ZSH_CACHE_DIR="$HOME/.cache/zsh"
export ZPLUG_LOADFILE="$ZSH_CONF/plugins.zsh"
source $ZPLUG/init.zsh

if ! zplug check; then
    zplug install
fi

# use --verbose to list loaded plugins
zplug load #--verbose

# Alias
source $ZSH_CONF/alias.zsh
# Auto-Completion
source $ZSH_CONF/completions.zsh
# fuzzy-search/completions etc.
source $ZSH_CONF/fzf.zsh
# Wort vor- und zurück springen
source $ZSH_CONF/keybindings.zsh
# History-Einstellungen etc.
source $ZSH_CONF/options.zsh
# Farben
source $ZSH_CONF/colors.zsh

if [[ -s "$HOME/.zlocal" ]]; then
  source "$HOME/.zlocal"
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f $BASEDIR/.dotfiles/.p10k.zsh ]] || source $BASEDIR/.dotfiles/.p10k.zsh


export PATH="$HOME/.jenv/bin:$PATH"
eval "$(jenv init -)"
export JAVA_HOME=`/usr/libexec/java_home -v 11.0`
eval $(docker-machine env default)
export PATH="/usr/local/opt/node@10/bin:$PATH"
